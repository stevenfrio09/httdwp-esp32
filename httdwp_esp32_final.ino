//WORKING
//READ BATTERY VOLTAGE
//READ FLOWSENSOR VALUE
//SWITCH DCBOOSTER ON OFF
//DEBOUNCE SWITCH
//SWITCH LCD& SENSORS ON/OFF
//SEND PUSH BUTTON STATUS TO FIREBASE
//TURN ON/OFF LCD&SENSORS ON/OFF FROM FIREBASE (PUSH BUTTON IS BUGGED)

#include <WiFi.h>
#include<FirebaseESP32.h> // firebase for arduino
#include<Wire.h>
#include<LiquidCrystal_I2C.h>


//water flow sensor variables
volatile int flow_frequency; // Measures flow sensor pulses
unsigned int l_hour; // Calculated litres/hour
#define flowsensor 27// 
unsigned long currentTime;
unsigned long cloopTime;


//wifi
#define WIFI_SSID "Guest_Who"
#define WIFI_PASSWORD "JollyHotdog"



//#define WIFI_SSID "Hello(2.4)"
//#define WIFI_PASSWORD "lrw1f1P4ssw0rd."




//firebase

String FIREBASE_USER_EMAIL = "johnstevenfrio@yahoo.com";
String FIREBASE_USER_EMAIL_FINAL;
String FIREBASE_HOST = "httdwp.firebaseio.com";
String FIREBASE_AUTH  = "hgFmnKticEcN480AFabQdPfDsRav4eQuaaXEPOza";
String firebase_battery_capacity;
String firebase_charging_internal_battery;
String firebase_discharging;
String firebase_flow_rate;
String firebase_voltage;
String firebase_relay_status;
String firebase_arduino_status;
String firebase_bat_value;
FirebaseData fbdo;



int sensor0;
int sensor1;
#define batPin 39
#define vsPin 36

//LCD
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4);


int lcdBatt;
int lcdInputVoltage;
int lcdDeviceStatus;
int lcdFlowRate0;

String initialBatValue = "120";


//powerboost
float RatioFactor = 1.68;
float originalBATVoltage = 0.0;
float originalVSVoltage = 0.0;


//RELAY
//#define RELAY_PIN D4

////transistor
#define dcboostPin 33

const int buttonPin = 32;    // the number of the pushbutton pin
const int modulePin = 25;      // the number of the LED pin


volatile int modulesState = LOW;
int switchState;
volatile int lastButtonState = HIGH;
volatile bool comingFromFirebase = LOW; //set default value to LOW. If LOW, it means the turn on/off command is coming from push button.If HIGH, it means turn on/off is coming from Firebase

volatile int firebaseModuleState = LOW; //set default value to LOW. It automatically sets modules state to LOW on init.

unsigned long timeNow;

unsigned long onTime;
unsigned long offTime;

unsigned long var = 0;


byte bat100[8] = {
  B01110,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000
};
byte bat80[8] = {
  B01110,
  B10001,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000
};
byte bat60[8] = {
  B01110,
  B10001,
  B10001,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000
};
byte bat40[8] = {
  B01110,
  B10001,
  B10001,
  B10001,
  B11111,
  B11111,
  B11111,
  B00000
};
byte bat20[8] = {
  B01110,
  B10001,
  B10001,
  B10001,
  B10001,
  B11111,
  B11111,
  B00000
};
byte bat0[8] = {
  B01110,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B11111,
  B00000
};

byte batX[8] = {
  B01110,
  B10001,
  B11011,
  B10101,
  B11011,
  B10001,
  B11111,
  B00000
};

void flow () // Interrupt function
{
  flow_frequency++;
}
void setup() {

  //init serial monitor
  Serial.begin(9600);


  FIREBASE_USER_EMAIL_FINAL  = "LnEDeWEXmje7eZ6SsxljSsvRxYY2";


  firebase_battery_capacity = FIREBASE_USER_EMAIL_FINAL  + "/0/battery_capacity";
  firebase_charging_internal_battery = FIREBASE_USER_EMAIL_FINAL + "/0/charging_internal_battery";
  firebase_discharging = FIREBASE_USER_EMAIL_FINAL  + "/0/discharging";
  firebase_flow_rate = FIREBASE_USER_EMAIL_FINAL  + "/0/flow_rate";
  firebase_voltage = FIREBASE_USER_EMAIL_FINAL  + "/0/voltage";
  firebase_relay_status = FIREBASE_USER_EMAIL_FINAL  + "/0/relay_status";
  firebase_arduino_status = FIREBASE_USER_EMAIL_FINAL  + "/0/module_status";
  firebase_bat_value = FIREBASE_USER_EMAIL_FINAL  + "/0/bat_value";

  //=============================================SETUP MULTIPLEXER=========================================================
  pinMode(batPin, INPUT);
  pinMode(vsPin, INPUT);


  //=============================================SETUP dcboostPin=========================================================
  pinMode(dcboostPin, OUTPUT);
  digitalWrite(dcboostPin, LOW);


  //=============================================SETUP modulesPin=========================================================

  pinMode(buttonPin, INPUT);
  pinMode(modulePin, OUTPUT);


  attachInterrupt(digitalPinToInterrupt(buttonPin), turnModulesOnOff, RISING); // Setup Interrupt










  //=============================================SETUP WIFI=========================================================
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);


  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }

  Serial.println("Connected. MAC Address: " + String(WiFi.macAddress()));




  //=============================================SETUP WATER FLOW SENSOR=========================================================
  pinMode(flowsensor, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(flowsensor), flow, FALLING); // Setup Interrupt
  sei(); // Enable interrupts
  currentTime = millis();
  cloopTime = currentTime;


  //=============================================SETUP FIREBASE=========================================================
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH); //init firebase
  //Set database read timeout to 1 minute (max 15 minutes)
  Firebase.setReadTimeout(fbdo, 1000 * 60);
  //tiny, small, medium, large and unlimited.
  //Size and its write timeout e.g. tiny (1s), small (10s), medium (30s) and large (60s).
  Firebase.setwriteSizeLimit(fbdo, "tiny");
  Firebase.reconnectWiFi(true);


  setFirebaseStringData(firebase_arduino_status, "OFF");//set default modules status to OFF
  setFirebaseStringData(firebase_relay_status, "ON");
  //  setFirebaseStringData(firebase_bat_value, initialBatValue); //set default bat value



}


void loop() {



  unsigned long lastSwitchTime  = 0;
  const unsigned long fiveMinutes = 1 * 60 * 1000;
  static unsigned long lastSampleTime = 0 - fiveMinutes;



  //check if state of modules is HIGH, if high run codes inside.
  //lcd begin will only be called/reinitialized once every five minutes after the lcd is turned ON.


  onTime = millis();

  offTime = millis();

  if (modulesState == HIGH) {
    lcd.createChar(0, bat100);
    lcd.createChar(1, bat80);
    lcd.createChar(2, bat60);
    lcd.createChar(3, bat40);
    lcd.createChar(4, bat20);
    lcd.createChar(5, bat0);
    lcd.createChar(6, batX);
    lcd.begin();
    lcd.backlight();

    lcd.clear();

    lcd.setCursor(7, 0);
    lcd.print("HTTDWP");

    if (l_hour < 10) {
      lcd.setCursor(3, 1);

    } else if (l_hour >= 10 && l_hour <= 99) {

      lcd.setCursor(2, 1);

    } else if (l_hour > 99) {
      lcd.setCursor(1, 1);

    }

    lcd.print((String)l_hour);
    lcd.setCursor(5, 1);
    Serial.println((String)l_hour);
    lcd.print("L/min");

    setLCDValues();
    readFlowSensor();
    captureSwitchValue();
    getBatteryVoltage();





  } else if (modulesState == LOW) {

    if (offTime - lastSampleTime >= fiveMinutes) {
      lastSampleTime += fiveMinutes;
      var = lastSampleTime;
      Serial.print("currentButtonState is LOW :");
      Serial.println(var);
    }

  }



  dcboostSwitch();
  captureSwitchValue();







  //
  //
  //  if (modulesState == HIGH) {
  //
  //    timeNow = millis();
  //    if (timeNow  - lastSampleTime >= fiveMinutes) {
  //      lastSampleTime += fiveMinutes;
  //      lcd.begin();
  //      lcd.backlight();
  //      lcd.clear();
  //
  //      setLCDValues();
  //    }
  //
  //    readFlowSensor();
  //    getBatteryVoltage();
  //  }
  //
  //
  //  dcboostSwitch();
  //  captureSwitchValue();

}


void turnModulesOnOff() {

  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();

  if (interrupt_time - last_interrupt_time > 1000) {

    //comingFromFirebase = false;
    comingFromFirebase = LOW;
    if (comingFromFirebase == LOW) {
      if (lastButtonState == LOW) {
        modulesState = HIGH;
        lastButtonState = HIGH;
        firebaseModuleState = HIGH;


      } else if (lastButtonState == HIGH) {
        modulesState = LOW;
        lastButtonState  = LOW;
        firebaseModuleState = LOW;

      }
      // digitalWrite(modulePin, modulesState); //uncommenting this line will disable oscillating LCD from HIGH to LOW to HIGH or vv. But will add delay when turning LCD to HIGH/LOW.
    }
  }
  last_interrupt_time = interrupt_time;

}


void dcboostSwitch() {

  if (Firebase.getString(fbdo, firebase_relay_status)) {
    String relayStatus = fbdo.stringData();


    if (relayStatus.equals("OFF")) {
      digitalWrite(dcboostPin, HIGH);
    }
    if (relayStatus.equals("ON")) {
      digitalWrite(dcboostPin, LOW);
    }
  }
}
















void captureSwitchValue() {

  //SEND DATA TO FIREBASE

  String firebaseModuleStatus;

  if (Firebase.getString(fbdo, firebase_arduino_status)) {

    firebaseModuleStatus = fbdo.stringData();

  }


  comingFromFirebase == LOW;
  if (comingFromFirebase == LOW) {
    //check if firebase data and current push button state is different. if so, send data to firebase
    if (firebaseModuleStatus == "ON" && firebaseModuleState == LOW) {

      setFirebaseStringData(firebase_arduino_status, "OFF");

    } else if (firebaseModuleStatus == "OFF" && firebaseModuleState == HIGH) {

      setFirebaseStringData(firebase_arduino_status, "ON");
    }
  }


  //comingFromFirebase = LOW;
  comingFromFirebase = HIGH;
  if (comingFromFirebase == HIGH) {
    if (firebaseModuleStatus == "ON") {

      Serial.println("captureSwitchValue||comingFromFirebase == HIGH||firebaseModuleStatus==ON");


      modulesState = HIGH;
      lastButtonState = HIGH;
      firebaseModuleState = HIGH;
      comingFromFirebase = HIGH;




    } else if (firebaseModuleStatus == "OFF" ) {

      Serial.println("captureSwitchValue||comingFromFirebase == HIGH||firebaseModuleStatus==OFF");
      modulesState = LOW;
      lastButtonState = LOW;
      firebaseModuleState = LOW;
      comingFromFirebase = HIGH;
    }
    digitalWrite(modulePin, modulesState);
  }

}














































////5 methods starts here

//=============================================read flow sensor values=========================================================
void readFlowSensor() {
  currentTime = millis();
  // Every second, calculate and print litres/hour
  if (currentTime >= (cloopTime + 1000)) {
    cloopTime = currentTime; // Updates cloopTime
    // Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min.
    l_hour = (flow_frequency * 1 / 9.5); // (Pulse frequency x 1 min) / 7.5Q = flowrate in L/min
    flow_frequency = 0; // Reset Counter


    //set lcd values

    if (l_hour > 0) {

      if (l_hour < 10) {
        lcd.setCursor(3, 1);

      } else if (l_hour >= 10 && l_hour <= 99) {

        lcd.setCursor(2, 1);

      } else if (l_hour > 99) {
        lcd.setCursor(1, 1);

      }

      lcd.print((String)l_hour);
      lcd.setCursor(5, 1);
      Serial.println((String)l_hour);
      lcd.print("L/min");

    } else {
      lcd.setCursor(3, 1);
      lcd.print("0");
      lcd.setCursor(5, 1);
      lcd.print("L/min");
    }

    //send data to firebase

  }
  setFirebaseStringData(firebase_flow_rate, (String)l_hour);
}






//=============================================GET POWERBOOST VS, BAT PIN VALUES AND MEASURE VOLTAGES =========================================================
void getBatteryVoltage() {
  sensor0 = analogRead(batPin);
  sensor1 = analogRead(vsPin);

  float VSvoltage = 0.0, rawVSvalue = 0.0, VSvalue = 0.0;
  float VSactualReadingToarduinoReadingRatio = 1.66;                  //===================Correct VS Arduino Reading to actual reading

  float BATvoltage = 0.0, rawBATvalue = 0.0, BATvalue = 0.0;
  float BATactualReadingToarduinoReadingRatio = 1.5;                  //===================Correct BAT Arduino Reading to actual reading


  for (unsigned int i = 0; i < 100000; i++) {
    rawBATvalue = rawBATvalue + (int)(sensor0 / 4);                       //===================Read BAT VALUE
    //Serial.println("rawBATvalue: " + (String)rawBATvalue);
    rawVSvalue = rawVSvalue + (int)(sensor1 / 4);                         //===================Read VS Value
    //Serial.println("rawVSvalue: " + (String)rawVSvalue);
  }


  //GET BAT VALUE
  rawBATvalue = (float)rawBATvalue / 100000.0;                        //===================Find average of BAT VALUES
  BATvalue = (float)((rawBATvalue / 1024.0) * 5) / BATactualReadingToarduinoReadingRatio; //Convert Voltage in 5v factor
  originalBATVoltage = (BATvalue * 1.75);
  BATvoltage = BATvalue;

  //GET VS VALUE
  rawVSvalue = (float)rawVSvalue / 100000.0;                          //===================Find average of VS values
  VSvalue = (float)((rawVSvalue / 1024.0) * 5) / VSactualReadingToarduinoReadingRatio; //Convert Voltage in 5v factor
  originalVSVoltage = VSvalue * 1.95;
  VSvoltage = VSvalue;


  Serial.println("RAW BAT VALUE: " + (String)rawBATvalue);
  Serial.println("Original Voltage: " + String(originalBATVoltage));
  Serial.println("BATvoltage: " + String(BATvoltage));


  Serial.println("******************************************");




  //=============================================RANGE AND ASSIGN VOLTAGES TO RESPECTIVE PERCENTAGE=========================================================

  float bat0 = 1.79,
        bat20 = 1.95,
        bat40 = 2.11,
        bat60 = 2.27,
        bat80 = 2.43,
        bat100 = 2.59;

  lcd.setCursor(0, 0);
  if ((BATvoltage <= bat100) && (BATvoltage > bat80)) {

    lcdBatt = 100;


    digitalWrite(dcboostPin, LOW);
    setFirebaseStringData(firebase_relay_status, "OFF");


  } else if ((BATvoltage <= bat80) && (BATvoltage > bat60)) {


    lcdBatt = 80;


  } else if ((BATvoltage <= bat60) && (BATvoltage > bat40)) {


    lcdBatt = 60;

  } else if ((BATvoltage <= bat40) && (BATvoltage > bat20)) {


    lcdBatt = 40;


  } else if ((BATvoltage <= bat20) && (BATvoltage > bat0)) {

    lcdBatt = 20;

  } else if ((BATvoltage <= bat0)) {

    lcdBatt = 0;

  }



  Serial.println("RAW VS VALUE: " + (String)rawVSvalue);
  if (((abs(rawBATvalue - rawVSvalue)) < 5) || (rawVSvalue <= 545)) {     //=============if vs and bat pin are the same, or vs pin is less  than 3.01v, not charging
    Serial.println("Not charging...");

    lcdInputVoltage = 0;

    setFirebaseStringData(firebase_voltage, "0");

  }



  if ((abs(rawBATvalue - rawVSvalue)) < 5) {
    //    lcd.setCursor(0, 3);
    //    lcd.print("                    ");

    lcdInputVoltage = 0;
    lcdDeviceStatus = 0;


    setFirebaseStringData(firebase_charging_internal_battery, "false");
    setFirebaseStringData(firebase_discharging, "false");


    setFirebaseStringData(firebase_voltage, "0");

    setFirebaseIntData(firebase_bat_value, lcdBatt);

  } else if (((rawBATvalue - rawVSvalue) >= 50) && rawBATvalue > rawVSvalue) {



    lcdInputVoltage = 0;

    setFirebaseStringData(firebase_voltage, "0");


    lcdDeviceStatus = 1;


    setFirebaseStringData(firebase_charging_internal_battery, "false");
    setFirebaseStringData(firebase_discharging, "true");
    //setFirebaseStringData(firebase_voltage, (String)originalVSVoltage);

    int firebaseBatValue;

    if (Firebase.getInt(fbdo, firebase_bat_value)) {

      firebaseBatValue = fbdo.intData();

    }

    lcdBatt = firebaseBatValue;

    setFirebaseStringData(firebase_battery_capacity, (String)lcdBatt);




  } else if (((rawVSvalue - rawBATvalue) >= 100) && rawVSvalue > rawBATvalue) {
    //    lcd.setCursor(0, 3);
    //    lcd.print("                    ");



    Serial.println("Original Voltage: " + String(originalVSVoltage));
    Serial.println("VSvoltage: " + String(VSvoltage));


    lcdDeviceStatus = 2;

    setFirebaseStringData(firebase_charging_internal_battery, "true");
    setFirebaseStringData(firebase_discharging, "false");
    setFirebaseStringData(firebase_voltage, (String)originalVSVoltage);

    setFirebaseIntData(firebase_bat_value, lcdBatt);


  } else if ((originalVSVoltage > 4.21) && rawVSvalue > rawBATvalue) {


    lcdInputVoltage = 1;

    lcdDeviceStatus = 3;
    setFirebaseStringData(firebase_charging_internal_battery, "true");
    setFirebaseStringData(firebase_discharging, "true");
    setFirebaseStringData(firebase_voltage, (String)originalVSVoltage);


    setFirebaseIntData(firebase_bat_value, lcdBatt);


  }


  Serial.println("==========================================");

  setFirebaseStringData(firebase_battery_capacity, (String)lcdBatt);                      //===================send data to firebase

}





//=============================================SET FIREBASE DATA=========================================================
void setFirebaseStringData(String address, String value) {


  Firebase.setString(fbdo, address, value);


}

void setFirebaseIntData(String address, int value) {
  Firebase.setInt(fbdo, address, value);
}


//void setLCD
void setLCDValues() {

  //  lcd.setCursor(0, 0);
  //  lcd.print("Bat. Cap.:");
  //
  //  lcd.setCursor(15, 0);
  //  lcd.print("%");


  //lcd.setCursor(0, 1);
  //lcd.print("Flow Rate:");
  //
  //  lcd.setCursor(14, 1);
  //  lcd.print(" L/min");
  //
  //  lcd.setCursor(0, 2);
  //  lcd.print("V. Input:");



  switch (lcdBatt) {

    case 100:
      //@bat100
      lcd.setCursor(12, 1);
      lcd.write((uint8_t)0);
      lcd.setCursor(13, 1);
      lcd.print(" 100%");
      break;
    case 80:

      //@bat80

      lcd.setCursor(12, 1);
      lcd.write((uint8_t)1);
      lcd.setCursor(13, 1);
      lcd.print(" 80%");
      break;

    case 60:
      //@bat60

      lcd.setCursor(12, 1);
      lcd.write((uint8_t)2);
      lcd.setCursor(13, 1);
      lcd.print(" 60%");
      break;

    case 40:
      //@bat40
      lcd.setCursor(12, 1);
      lcd.write((uint8_t)3);
      lcd.setCursor(13, 1);
      lcd.print(" 40%");
      break;
    //@bat20
    case 20:
      lcd.setCursor(12, 1);
      lcd.write((uint8_t)4);
      lcd.setCursor(13, 1);
      lcd.print(" 20%");
      break;

    case 0:
      //@batt0
      lcd.setCursor(12, 1);
      lcd.write((uint8_t)5);
      lcd.setCursor(13, 1);
      lcd.print(" 0%");
      break;

    case 99:
      lcd.setCursor(12, 1);
      lcd.write((uint8_t)6);
      lcd.setCursor(13, 1);
      lcd.print(" NB");
      break;
  }



  switch (lcdInputVoltage) {

    case 0:
      lcd.setCursor(2, 2);
      lcd.print("No Input Voltage");
      break;
    case 1:

      lcd.setCursor(3, 2);
      lcd.print("V. Input:");
      lcd.setCursor(12, 2);
      lcd.print(originalVSVoltage);
      lcd.setCursor(16, 2);
      lcd.print("v");
      break;
  }


  switch (lcdDeviceStatus) {
    case 0:
      setLine4Message("     Internal battery and external device not charging");
      break;
    case 1:
      setLine4Message("     External device charging from internal battery");
      break;
    case 2:
      setLine4Message("     Charging internal battery");
      break;
    case 3:
      setLine4Message("     External device charging directly from HTTDWP");
      break;

  }


}
void setLine4Message(char * message) {
  char * messagePadded = message;
  for (int letter = 0; letter <= strlen(messagePadded) - 20; letter++) //From 0 to upto n-16 characters supply to below function
  {
    lcd.setCursor(0, 3);
    for (int thisLetter = letter; thisLetter <= letter + 19; thisLetter++) // Print only 16 chars in Line #2 starting 'startLetter'
    {
      lcd.print(messagePadded[thisLetter]);
    }
    delay(150);
  }
}






//5 methods ends here
